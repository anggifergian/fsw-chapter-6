const express = require('express');
const router = express.Router();

router.post('/', (req, res) => {
    const {username, password} = req.body;
    if (username === 'admin' && password === 'admin') {
        res.redirect('/players');
    } else {
        res.status(404).send(`Invalid username and password`);
    }
})

module.exports = router;