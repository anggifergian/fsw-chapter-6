const express = require('express');
const router = express.Router();

const Pool = require('pg').Pool;

const pool = new Pool({
    user: "postgres",
    password: "",
    host: "localhost",
    port: "5432",
    database: "binarbootcamp"
});


router.get('/', async (req, res) => {
    return await pool.query(
        'SELECT * FROM user_game', (err, result) => {
            if (err) {
                res.status(404).json(err);
            }
            res.render('players/index', { title: 'Create Players', data: result.rows});
        }
    )
})

router.post('/', async (req, res) => {
    const { username, password } = req.body;
    return await pool.query(
        'INSERT INTO user_game (username, password) VALUES ($1, $2) RETURNING *',
        [username, password],
        (err, result) => {
            if (err) {
                res.status(500).json(err);
            } else if (!Array.isArray(result.rows) || result.rows.length < 1) {
                res.status(500).json(err);
            }
            res.redirect('players');
        }
    )
})

router.put('/:id', async (req, res) => {
    const id = parseInt(req.params.id);
    const { username, password } = req.body;

    return await pool.query(
        'UPDATE user_game SET username = $1, password = $2 WHERE id = $3 returning *', [username, password, id],
        (err, result) => {
            if (err) {
                res.status(500).json(err)
            }
            if (typeof result.rows == 'undefined') {
                res.status(404).send(`Resource not found`);
            } else if (!Array.isArray(result.rows) || result.rows.length < 1) {
                res.status(404).send(`Username not found`);
            } else {
                res.status(200).send(`Username modified with ID: ${result.rows[0].id}`);
            }
        }
    )
})

router.delete('/:id', async (req ,res) => {
    const id = parseInt(req.params.id);
    return await pool.query(
        'DELETE FROM user_game WHERE id = $1', [id],
        (err, result) => {
            try {
                if (err) {
                    return res.status(500).json(err);
                } else if (result.rowCount === 0) {
                    return res.send(`Players has been deleted`);
                }
                res.status(200).send(`Players deleted with ID: ${id}`);
            } catch (error) {
                console.error(error.message);
            }
        }
    )
})

module.exports = router;