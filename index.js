const bodyParser = require('body-parser');
const express = require('express');
const PORT = process.env.PORT || 3000;
const app = express();


// MIDDLEWARE
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.urlencoded({ extended: true }));
app.use(express.static('public'));
app.set('view engine', 'ejs');


// ROUTER
app.use('/login', require('./controller/login'));
app.use('/players', require('./controller/players'));
app.get('/', (req, res) => {
    res.render('login/login');
});


// LISTEN
app.listen(PORT, () => {
    console.log(`Listening on port ${PORT}`);
})