CREATE TABLE user_game (
    id SERIAL NOT NULL PRIMARY KEY,
    username VARCHAR(50) NOT NULL,
    password VARCHAR(20) NOT NULL
);

CREATE TABLE user_game_biodata (
    id SERIAL PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    email VARCHAR(30) NOT NULL,
    user_id INT,
    CONSTRAINT fk_user FOREIGN KEY (user_id) REFERENCES user_game(id)
);

CREATE TABLE user_game_history (
    id SERIAL PRIMARY KEY,
    level VARCHAR(20),
    score INT,
    user_id INT,
    CONSTRAINT fk_user FOREIGN KEY (user_id) REFERENCES user_game(id)
);