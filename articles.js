const Pool = require('pg').Pool;

const pool = new Pool({
    user: "postgres",
    password: "",
    host: "localhost",
    port: "5432",
    database: "binarbootcamp"
});

const listPlayers = async (req, res) => {
    const id = parseInt(req.params.id);
    return await pool.query(
        'SELECT * FROM player_score WHERE id = $1', [id],
        (error, results) => {
            console.log(results.rows);
            if (error) {
                res.status(500).json(error);
            }
            res.render('players/create-form', { data: results.rows});
        })
}
const createArticles = async (req, res) => {
    const { name } = req.body.name;
    const { score } = req.body.score;
    return await pool.query(
        'INSERT INTO player_score (name, score) VALUES (($1), ($2)) RETURNING *', [name, score],
        (error, result) => {
            if (error) {
                response.status(500).json(error);
            } else if (!Array.isArray(result.rows) || result.rows.length < 1) {
                response.status(500).json(error);
            }
            res.status(201).send(`Players add with ID: ${result.rows[0].id}`);
        });
}

const getLessThan = async (req, res) => {
    const id = parseInt(req.params.id);
    return await pool.query(
        'SELECT * FROM player_score WHERE id <= $1', [id],
        (err, result) => {
            try {
                if (err) {
                    res.status(500).json(err)
                } else if (!Array.isArray(result.rows) || result.rows.length < 1) {
                    res.status(404).send(`Data less than and equals to ID: ${id} is not found.`)
                }
                res.status(200).json(result.rows);
            } catch (error) {
                console.log(error);
            }
        }
    )
}

module.exports = { createArticles, getLessThan, listPlayers };