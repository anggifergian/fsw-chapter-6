const Pool = require('pg').Pool;

const pool = new Pool({
    user: "postgres",
    password: "",
    host: "localhost",
    port: "5432",
    database: "binarbootcamp"
});

const getUserLogin = async (req, res) => {
    return await pool.query(
        'SELECT * FROM user_game',
        (err, result) => {
            if (err) return res.status(404).json(err);
            res.status(200).send(result.rows);
        }
    )
}

module.exports = { getUserLogin }
