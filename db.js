const Pool = require('pg').Pool;

const pool = new Pool({
    user: "postgres",
    password: "",
    host: "localhost",
    port: "5432",
    database: "binarbootcamp"
});

const getPlayers = async (request, response) => {
    return await pool.query(
        'SELECT * FROM player_score',
        (error, results) => {
        if (error) {
            response.status(500).json(error);
        }
        response.status(200).json(results.rows);
    })
}

const getPlayersById = async (request, response) => {
    const id = parseInt(request.params.id);
    return await pool.query(
        'SELECT * FROM player_score WHERE id=($1)', [id],
        (error, result) => {
        try {
            if (error) {
                response.status(500).json(error);
            } else if (!Array.isArray(result.rows) || result.rows.length < 1) {
                response.status(404).send(`Players with ID: ${id} is not found.`)
            }
            response.status(200).json(result.rows);
        } catch (error) {
            console.error(error.message);
        }
    });
}

const createPlayers = async (request, response) => {
    const { name, score } = request.body;

    return await pool.query(
        'INSERT INTO player_score (name, score) VALUES (($1), ($2)) RETURNING *', [name, score],
        (error, result) => {
        if (error) {
            response.status(500).json(error);
        } else if (!Array.isArray(result.rows) || result.rows.length < 1) {
            response.status(500).json(error);
        }
        response.status(201).send(`Players add with ID: ${result.rows[0].id}`);
    });
}

const updatePlayers = async (request, response) => {
    const id = parseInt(request.params.id);
    const { name, score } = request.body;

    return await pool.query(
        'UPDATE player_score SET name = $1, score = $2 WHERE id = $3 returning *', [ name, score, id ],
        (err, result) => {
            if (err) {
                response.status(500).json(err)
            }
            if (typeof result.rows == 'undefined') {
                response.status(404).send(`Resource not found`);
            } else if (!Array.isArray(result.rows) || result.rows.length < 1) {
                response.status(404).send(`Players not found`);
            } else {
                response.status(200).send(`Players modified with ID: ${result.rows[0].id}`);
            }
        }
    )
}

const deletePlayer = async (request, response) => {
    const id = parseInt(request.params.id);
    return await pool.query(
        'DELETE FROM player_score WHERE id = $1', [id],
        (err, result) => {
            if (err) {
                response.status(500).json(err);
            }
            response.status(200).send(`Players deleted with ID: ${id}`);
        }
    )
}

module.exports = { getPlayers, getPlayersById, createPlayers, updatePlayers, deletePlayer };